#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "proc_mat.h"
#include "gen_rand.h"
#include <string.h>
#include <omp.h>
#include <time.h>
double proc_mat_func(int n);

double my_max(double array, int n);


int main( int argc, char** argv){

char* nn = argv[1];
char* mm = argv[2];
int n = atoi(nn)-1;
int m = atoi(mm);

int seed = time(NULL);
srand(seed);

for (int i=0; i<m; i++){

proc_mat_func(n);
}

  double* MX;
  double* MY;
  double* MXX;
  double* MXY;
  double * MYY;
  double* RHOHATMAX;

FILE* dfile = fopen("data.txt", "r");

int p = 0;
char c;
int n1;

while(!feof(dfile))
{
  c = fgetc(dfile);
  if(c == '\n')
  {
   p++;
   n1 = p;
  }
}
    fclose(dfile); 

FILE* ddfile = fopen("data.txt", "r");

 MX = (double*) malloc(m * sizeof(double));

 MY = (double*) malloc(m * sizeof(double));

 MXX = (double*) malloc(m * sizeof(double));

 MXY = (double*) malloc(m * sizeof(double));
 
 MYY = (double*) malloc(m * sizeof(double));
 
 RHOHATMAX = (double*) malloc(m * sizeof(double));


for ( int i = 0; i < n1; i++ ) 
 {
   fscanf( ddfile, "%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n", MX+i, MY+i, MXX+i, MXY+i, MYY+i, RHOHATMAX+i);

}
 fclose(ddfile);

///////////////////////REPORTING THE MATRIX SIZE AND NUMBER OF MONTE CARLO SIMULATIONS///////////////////
printf("The size of the matrix is %d by %d, and %d number of Monte Carlo simulations\n", (n+1), (n+1), m);

/////////////////PRINTING THE DATA FOR THE SIX MEASUREMENTS AFTER EACH MONTE CARLO SIMULATIONS///////////////
printf("MX\t\tMY\t\tMXX\t\tMXY\t\tMYY\t\tRHOHATMAX\n");
for (int j =0; j<m; j++)
printf("%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n", MX[j], MY[j], MXX[j], MXY[j], MYY[j], RHOHATMAX[j]);

/////////////////////CALCULATING THE MEANS OF THE SIX MEASUREMENTS/////////////////////////
double MMX = 0.0;
double MMY = 0.0;
double MMXX = 0.0;
double MMXY = 0.0;
double MMYY = 0.0;
double MRHOHATMAX = 0.0;

#pragma omp parallel for reduction(+:MMX,MMY,MMXX,MMXY,MMYY,MRHOHATMAX)
for (int k=0; k<m; k++){

MMX += MX[k];
MMY += MY[k];
MMXX += MXX[k];
MMXY += MXY[k];
MMYY += MYY[k];
MRHOHATMAX += RHOHATMAX[k];

}

MMX = MMX/(m);
MMY = MMY/(m);
MMXX = MMXX/(m);
MMXY = MMXY/(m);
MMYY = MMYY/(m);
MRHOHATMAX = MRHOHATMAX/(m);


//////////PRINTING THE MEAN OF THE SIX MEASUREMENTS//////////

printf("mean of six measurements\n %lf\t%lf\t%lf\t%lf\t%lf\t%lf\n", MMX, MMY, MMXX, MMXY, MMYY, MRHOHATMAX);


///////////CALCULATING THE STANDARD DEVIATIONS OF THE SIX MEASUREMENTS///////////////////

double SMX = 0.0;
double SMY = 0.0;
double SMXX = 0.0;
double SMXY = 0.0;
double SMYY = 0.0;
double SRHOHATMAX = 0.0;

#pragma omp parallel for reduction(+:SMX,SMY,SMXX,SMXY,SMYY,SRHOHATMAX)
for (int l=0; l<m ; l++){

SMX += (MX[l] - MMX)*(MX[l] - MMX);
SMY += (MY[l] - MMY)*(MY[l] - MMY);
SMXX += (MXX[l] - MMXX)*(MXX[l] - MMXX);
SMXY += (MXY[l] - MMXY)*(MXY[l] - MMXY);
SMYY += (MYY[l] - MMYY)*(MYY[l] - MMYY);
SRHOHATMAX += (RHOHATMAX[l] - MRHOHATMAX)*(RHOHATMAX[l] - MRHOHATMAX);

}

SMX = sqrt(SMX/(m-1));
SMY = sqrt(SMY/(m-1));
SMXX = sqrt(SMXX/(m-1));
SMXY = sqrt(SMXY/(m-1));
SMYY = sqrt(SMYY/(m-1));
SRHOHATMAX = sqrt(SRHOHATMAX/(m-1));

////////////////////////PRINTING THE STD OF THE SIX MEASUREMENTS////////////////
printf("standard deviation\n %lf\t%lf\t%lf\t%lf\t%lf\t%lf\n", SMX, SMY, SMXX, SMXY, SMYY, SRHOHATMAX);

return 0;
}
