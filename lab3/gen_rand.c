#include "gen_rand.h"
#include <stdlib.h>
#include <math.h>
#include "proc_mat.h"
#include <stdio.h>

Real gen_rand_normal(const Real mean, const Real std) {
  Real u1, u2, w;
  do {
     u1 = 2.0*(-0.5+((double) rand()/RAND_MAX));
     u2 = 2.0*(-0.5+((double) rand()/RAND_MAX));
     w = u1 * u1 + u2 * u2;
    }
    while ( w>=1.0 || w==0.0 );
    return mean + std * u1 * sqrt((-2.0*log(w))/w);
} 


