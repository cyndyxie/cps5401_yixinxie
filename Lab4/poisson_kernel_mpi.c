#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <float.h>
#include <time.h>

#include <mpi.h>

#define COLS 1200
#define ROWS COLS
#define SX 0.66611157
#define SY 0.29142381

double Temperature[ROWS+2][COLS+2];
double Temperature_last[ROWS+2][COLS+2];

// communication tags
#define DOWN     100
#define UP       101
#define RIGHT    200
#define LEFT     201

#define MAX_TEMP_ERROR 1e-5

int main(int argc, char *argv[]) {

  int max_iterations;
  int iteration=1;
  double dt;
  double dh = 1.0/(COLS+1);
  time_t begin, end;
  double elapsed_time;

  double dt_global=DBL_MAX; // delta t

  int global_iteration, global_i_min, global_j_min;
  double global_loc_min;
  int        npes;                // number of PEs
  int        my_PE_num;           // my PE number
  MPI_Status status;              // status returned by MPI calls

  MPI_Init(&argc, &argv);

  MPI_Comm_rank(MPI_COMM_WORLD, &my_PE_num);
  MPI_Comm_size(MPI_COMM_WORLD, &npes);

  int npedir = sqrt(npes);
  unsigned int nblocks = ROWS/npedir;
  unsigned int i_init = my_PE_num%npedir;
  unsigned int j_init = my_PE_num/npedir;

  if(my_PE_num == 0){
    printf("Maximum iterations [100-20000]?\n");
    fflush(stdout); // Not always necessary, but can be helpful
    scanf("%d", &max_iterations);
  }

  // bcast max iterations to other PEs
  MPI_Bcast(&max_iterations, 1, MPI_INT, 0, MPI_COMM_WORLD);

  begin = clock();

  // Step 1: Initialize
  memset( Temperature, 0, (ROWS+2)*(COLS+2)*sizeof(double) );
  memset( Temperature_last, 0, (ROWS+2)*(COLS+2)*sizeof(double) );

  // Step 2: Fixed point iteration
  while ( dt_global > MAX_TEMP_ERROR && iteration <= max_iterations ) {
    // main calculation: average my four neighbors
    for ( int i = i_init*nblocks + 1; i <= i_init*nblocks + nblocks; i++ ) {
      for ( int j = j_init*nblocks + 1; j <= j_init*nblocks + nblocks; j++ ) {
        double loc_x = i * dh;
        double loc_y = j * dh;
        double loc_source = 0.0;
        if ( (fabs( loc_x-SX ) < 0.1*dh) && (fabs( loc_y-SY ) < 0.1*dh) )
          loc_source = 1.0/dh/dh;
        Temperature[i][j] = 0.25 * ( Temperature_last[i+1][j] + Temperature_last[i-1][j] +
                                     Temperature_last[i][j+1] + Temperature_last[i][j-1] -
                                     dh * dh * loc_source );
      }
    }

    dt = 0.0;

    for ( int i = i_init*nblocks + 1; i <= i_init*nblocks + nblocks; i++ ) {
      for ( int j = j_init*nblocks + 1; j <= j_init*nblocks + nblocks; j++ ) {
        dt = fmax( fabs(Temperature[i][j]-Temperature_last[i][j]), dt);
        Temperature_last[i][j] = Temperature[i][j];
      }
    }

    if (npedir > 1 ){
      // COMMUNICATION PHASE: send ghost elements for next iteration
      for (int j = 1; j <= nblocks+1; j++){
        // send bottom real row down
        if (my_PE_num < npedir*(npedir-1)) {
          MPI_Send(&Temperature_last[i_init*nblocks + j][j_init*nblocks+nblocks], 1, MPI_DOUBLE, my_PE_num+npedir, DOWN, MPI_COMM_WORLD);
        }
        // receive the bottom row from above and put it in the top ghost row
        if (my_PE_num >= npedir) {
          MPI_Recv(&Temperature_last[i_init*nblocks + j][j_init*nblocks], 1, MPI_DOUBLE, my_PE_num-npedir, DOWN, MPI_COMM_WORLD, &status);
        }
        // send top real row up
        if (my_PE_num >= npedir) {
          MPI_Send(&Temperature_last[i_init*nblocks + j][j_init*nblocks + 1], 1, MPI_DOUBLE, my_PE_num-npedir, UP, MPI_COMM_WORLD);
        }
        // receive the top row from below and put it in the bottom ghost row
        if (my_PE_num < npedir*(npedir-1)) {
          MPI_Recv(&Temperature_last[i_init*nblocks + j][(j_init+1)*nblocks + 1], 1, MPI_DOUBLE, my_PE_num+npedir, UP, MPI_COMM_WORLD, &status);
        }
        // send right real column to the right
        if ((my_PE_num+1)%npedir != 0) {
          MPI_Send(&Temperature_last[(i_init+1)*nblocks][j_init*nblocks+j], 1, MPI_DOUBLE, my_PE_num+1, RIGHT, MPI_COMM_WORLD);
        }
        // receive the right column from left and put it in the left ghost column
        if (my_PE_num%npedir != 0) {
          MPI_Recv(&Temperature_last[i_init*nblocks][j_init*nblocks+j], 1, MPI_DOUBLE, my_PE_num-1, RIGHT, MPI_COMM_WORLD, &status);
        }
        // send left real column to the left
        if (my_PE_num%npedir != 0) {
          MPI_Send(&Temperature_last[i_init*nblocks+1][j_init*nblocks+j], 1, MPI_DOUBLE, my_PE_num-1, LEFT, MPI_COMM_WORLD);
        }
        // receive the left column from right and put it in the right ghost column
        if ((my_PE_num+1)%npedir != 0) {
          MPI_Recv(&Temperature_last[(i_init+1)*nblocks+1][j_init*nblocks+j], 1, MPI_DOUBLE, my_PE_num+1, LEFT, MPI_COMM_WORLD, &status);
        }
      }
    }

    MPI_Barrier(MPI_COMM_WORLD);

    MPI_Allreduce(&dt, &dt_global, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    // periodically print test values - only for PE in lower corner
    if ((iteration % 100) == 0){
      if(my_PE_num == 0)
        printf("Iteration %d: maximum difference: %f.\n", iteration, dt_global);
    }
    iteration++;
  }
  end = clock();
  elapsed_time = (double)(end-begin)/CLOCKS_PER_SEC;

  MPI_Barrier(MPI_COMM_WORLD);

  if(my_PE_num == 0){
    printf("\nMax error at iteration %d was %f.\n", iteration-1, dt_global);
    printf("Total time was %f seconds.\n", elapsed_time);
  }

  // Step 3: find the global minimum
  int i_min = 0, j_min = 0, final_i_min = 0, final_j_min = 0;
  double loc_min = 0.0;
  for ( int i = i_init*nblocks + 1; i <= i_init*nblocks + nblocks; i++ ){
    for ( int j = j_init*nblocks + 1; j <= j_init*nblocks + nblocks; j++ ) {
      if ( Temperature_last[i][j] < loc_min ) {
        loc_min = Temperature_last[i][j];
        i_min = i;
        j_min = j;
      }
    }
  }
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Allreduce(&loc_min, &global_loc_min, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
  if (loc_min == global_loc_min){
    printf("\nMinimum solution at grid point (%d,%d) with value: %f.\n", i_min ,j_min ,global_loc_min);
  }
  MPI_Finalize();
  return 0;
}
