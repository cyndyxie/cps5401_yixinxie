#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#define DIM = 20

extern void dposv_(char *UPLO, int *N, int *NRHS, double *A, int *LDA, double *B, int *LDB, int *INFO);
extern void dgesv_(int *N, int *NRHS, double *A, int *LDA, int *IPIV, double *B, int *LDB, int *INFO);
extern void dgeev_(char *JOBVL, char *JOBVR, int *N, double *A, int *LDA, double *WR, double *WI, double *VL, int *LDVL, double *VR, int *LDVR, double *WORK, int *LWORK, int *INFO);
extern void dsyev_(char *JOBZ, char *UPLO, int *N, double *A, int *LDA, double *W, double *WORK, int *LWORK, int *INFO);

double main( int argc, char** argv ) {
    double x, y, z;
    //define 20 functions as given
    double verse1(double x, double y, double z) {return 1}
    double verse2(double x, double y, double z) {return x;}
    double verse3(double x, double y, double z) {return y;}
    double verse4(double x, double y, double z) {return z;}
    double verse5(double x, double y, double z) {return x*x;}
    double verse6(double x, double y, double z) {return y*y;}
    double verse7(double x, double y, double z) {return z*z;}
    double verse8(double x, double y, double z) {return x*y;}
    double verse9(double x, double y, double z) {return x*z;}
    double verse10(double x, double y, double z) {return y*z;}
    double verse11(double x, double y, double z) {return x*x*x;}
    double verse12(double x, double y, double z) {return y*y*y;}
    double verse13(double x, double y, double z) {return z*z*z;}
    double verse14(double x, double y, double z) {return x*y*y;}
    double verse15(double x, double y, double z) {return x*x*y;}
    double verse16(double x, double y, double z) {return x*z*z;}
    double verse17(double x, double y, double z) {return x*x*z;}
    double verse18(double x, double y, double z) {return y*z*z;}
    double verse19(double x, double y, double z) {return y*y*z;}
    double verse20(double x, double y, double z) {return x*y*z;}
    //read data.txt file, and get x[], y[], z[], f[]
    double v[1000], x[1000], y[1000], z[1000], f[1000];
    double mat[DIM][DIM];
    int i = 0, j, k;
    FILE *fp;
    fp = fopen("data.txt", "r");
    if(fp == NULL)
        return -1;
    while(fscanf(fp, "%lf", &v[i]) != EOF) //read file till EOF
        i++;
    fclose(fp);
    //build 4 array: x,y,z,f
    for(j = 0; j < i; j ++)
    {   if(j%4 ==0)
        {
            x[k]=v[j];
        }
        if(j%4 ==1)
        {
            y[k]=v[j];
        }
        if(j%4 ==1)
        {
            z[k]=v[j];
        }
        if(j%4 ==3)
        {
            f[k]=v[j];
        }
    }
    
   
    //function pointer
    void pv(){
        double c;
        double (*p)(double, double, double);
        op0 = &verse1;
        c = (*p)(x,y,z);
        op1 = &verse2;
        op2 = &verse3;
        op3 = &verse4;
        op4 = &verse5;
        op5 = &verse6;
        op6 = &verse7;
        op7 = &verse8;
        op8 = &verse9;
        op9 = &verse10;
        op10 = &verse11;
        op11 = &verse12;
       }
       
    //function pointer array
    double (*op[20])(double x, double y, double z);
       op[0] = verse1;
       op[1] = verse2;
       op[2] = verse3;
       op[3] = verse4;
       op[4] = verse5;
       op[5] = verse6;
       op[6] = verse7;
       op[7] = verse8;
       op[8] = verse9;
       op[9] = verse10;
       op[10] = verse11;
       op[11] = verse12;
       op[12] = verse13;
       op[13] = verse14;
       op[14] = verse15;
       op[15] = verse16;
       op[16] = verse17;
       op[17] = verse18;
       op[18] = verse19;
       op[19] = verse20;
       printf("%d %d\n", op[0](numa1, numb1), op[1](numa2, numb2));
       getch();
       
    //calculate
    for (i=0; i<DIM; i++){
         c += (*p)(x[i],y[i],z[i]);
    }
    //new matrix
    for (i=0; i<DIM; i++){
        for (j=0; j<DIM; j++){
            mat[i][j]=op[i]op[j]
        }
    }
              
    //write A matrix values into matrix.txt, B vector values into vector.txt
        FILE *fpWrite=fopen("matrix.txt","w");
        if(fpWrite==NULL)
        {
            return 0;
        }
        for(int k=0;k<1000;k++)
            fprintf(fpWrite,"%lf ",x[k]);
        fclose(fpWrite);

   
    // symmetric matrix A
          double* A = (double*) malloc( DIM * DIM * sizeof( double ) );

          // Vector B
          double* B = (double*) calloc( DIM, sizeof( double ) );
          
          //get data from txt file
          FILE* data_file = fopen("matrix.txt", "r");
          for ( int i = 0; i < DIM; i++ )
            for ( int j = 0; j < DIM; j++ )
              fscanf( data_file, "%lf\t", A+i*DIM+j );
          fclose( data_file );

          data_file = fopen("vector.txt", "r");
          for ( int i = 0; i < DIM; i++ )
            for ( int j = 0; j < DIM; j++ )
              fscanf( data_file, "%lf\t", B+i*DIM+j );
          fclose( data_file );
          

   
    //matrix: solve Ax = B
    {
      char UPLO = 'L';
      int  N    = DIM;
      int  NRHS = 1;
      int  LDA  = DIM;
      int  LDB  = DIM;
      int  INFO;
      double* MAT = (double*) malloc( DIM * DIM * sizeof( double ) );
      double* RHS = (double*) malloc( DIM * sizeof( double ) );
      memcpy( MAT, A,  DIM * DIM * sizeof(double) );
      memcpy( RHS, B, DIM * sizeof(double) );
      dposv_(&UPLO, &N, &NRHS, &(MAT[0]), &LDA, &(RHS[0]), &LDB, &INFO);
      printf("\nPart 2-1: Solve Ax=B:\n");
      for ( int i = 0; i < DIM; i++ )
        printf("\t%f\n",RHS[i]);
      free( MAT );
      free( RHS );
    }
}

